import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WorkingHoursReportPage } from './working-hours-report';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { TranslateModule } from '@ngx-translate/core';
import { OnCreate } from './dummy-directive';

@NgModule({
  declarations: [
    WorkingHoursReportPage,
    OnCreate
  ],
  imports: [
    IonicPageModule.forChild(WorkingHoursReportPage),
    SelectSearchableModule,
    TranslateModule.forChild()
  ],
  exports: [
    OnCreate
  ]
})
export class WorkingHoursReportPageModule {}
